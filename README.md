# ASRM_project

This repos is for ASRM555 (Advaced predictive analysis) project

## Problem

The problem for this project is ATPA Sample Assessment (2023)
For more detail: please check https://www.soa.org/education/exam-req/edu-exam-atpa/
Any files, data used in this repo is from SOA (Society of Actuaries): https://www.soa.org/499563/globalassets/assets/files/edu/2022/atpa-sample-assessment.zip

## Contributing

This repo is contributed by Mai Pham
